import QtQuick 2.0
import Sailfish.Silica 1.0
import Sailfish.Share 1.0

Page {
    id: page

    property string res;

    allowedOrientations: Orientation.All

    PageHeader {
        title: qsTr("Share")
    }

    ShareAction {
        id: share
        resources: [res]
    }

    Component.onCompleted: share.trigger()
}
