<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1">
<context>
    <name>CreateRoom</name>
    <message>
        <location filename="../qml/pages/CreateRoom.qml" line="50"/>
        <source>Create Room</source>
        <translation type="unfinished">Créer un salon</translation>
    </message>
    <message>
        <location filename="../qml/pages/CreateRoom.qml" line="51"/>
        <source>Create</source>
        <translation type="unfinished">Créer</translation>
    </message>
    <message>
        <location filename="../qml/pages/CreateRoom.qml" line="56"/>
        <source>Visibility</source>
        <translation type="unfinished">Visibilité</translation>
    </message>
    <message>
        <location filename="../qml/pages/CreateRoom.qml" line="58"/>
        <source>Private</source>
        <translation type="unfinished">Privé</translation>
    </message>
    <message>
        <location filename="../qml/pages/CreateRoom.qml" line="59"/>
        <source>Public</source>
        <translation type="unfinished">Public</translation>
    </message>
    <message>
        <location filename="../qml/pages/CreateRoom.qml" line="70"/>
        <location filename="../qml/pages/CreateRoom.qml" line="71"/>
        <source>Name</source>
        <translation type="unfinished">Nom</translation>
    </message>
    <message>
        <location filename="../qml/pages/CreateRoom.qml" line="79"/>
        <source>Topic</source>
        <translation type="unfinished">Sujet</translation>
    </message>
    <message>
        <location filename="../qml/pages/CreateRoom.qml" line="80"/>
        <source>Topic (optional)</source>
        <translation type="unfinished">Sujet (optional)</translation>
    </message>
    <message>
        <location filename="../qml/pages/CreateRoom.qml" line="101"/>
        <location filename="../qml/pages/CreateRoom.qml" line="102"/>
        <source>Room alias</source>
        <translation type="unfinished">Alias du salon</translation>
    </message>
    <message>
        <location filename="../qml/pages/CreateRoom.qml" line="119"/>
        <source>Enable end-to-end encryption</source>
        <translation type="unfinished">Activer le chiffrement end-to-end</translation>
    </message>
    <message>
        <location filename="../qml/pages/CreateRoom.qml" line="120"/>
        <source>Bridges and most bots won&apos;t work yet.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Credits</name>
    <message>
        <location filename="../qml/pages/Credits.qml" line="10"/>
        <source>Credits</source>
        <translation type="unfinished">Crédits</translation>
    </message>
</context>
<context>
    <name>Invite</name>
    <message>
        <location filename="../qml/pages/Invite.qml" line="15"/>
        <source>Invite</source>
        <translation type="unfinished">Inviter</translation>
    </message>
    <message>
        <location filename="../qml/pages/Invite.qml" line="35"/>
        <source>You&apos;ve been invited to</source>
        <translation type="unfinished">On vous a invité à</translation>
    </message>
    <message>
        <location filename="../qml/pages/Invite.qml" line="35"/>
        <source> chat with</source>
        <translation type="unfinished"> charrer avec</translation>
    </message>
    <message>
        <location filename="../qml/pages/Invite.qml" line="51"/>
        <source>Join</source>
        <translation type="unfinished">Rejoindre</translation>
    </message>
    <message>
        <location filename="../qml/pages/Invite.qml" line="65"/>
        <source>Reject</source>
        <translation type="unfinished">Rejeter</translation>
    </message>
</context>
<context>
    <name>JoinPublicRoom</name>
    <message>
        <location filename="../qml/pages/JoinPublicRoom.qml" line="19"/>
        <source>Join Room</source>
        <translation type="unfinished">Rejoindre le salon</translation>
    </message>
    <message>
        <location filename="../qml/pages/JoinPublicRoom.qml" line="20"/>
        <source>Join</source>
        <translation type="unfinished">Rejoindre</translation>
    </message>
    <message>
        <location filename="../qml/pages/JoinPublicRoom.qml" line="78"/>
        <source> members</source>
        <translation type="unfinished"> membres</translation>
    </message>
</context>
<context>
    <name>LoginDialog</name>
    <message>
        <location filename="../qml/pages/LoginDialog.qml" line="28"/>
        <source>Login</source>
        <translation type="unfinished">Identifiant</translation>
    </message>
    <message>
        <location filename="../qml/pages/LoginDialog.qml" line="32"/>
        <source>Homeserver</source>
        <translation type="unfinished">Homeserver</translation>
    </message>
    <message>
        <location filename="../qml/pages/LoginDialog.qml" line="33"/>
        <source>Homeserver URL (matrix.org)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/LoginDialog.qml" line="41"/>
        <source>Username</source>
        <translation type="unfinished">Nom d’utilisateur</translation>
    </message>
    <message>
        <location filename="../qml/pages/LoginDialog.qml" line="42"/>
        <source>Username (user1)</source>
        <translation type="unfinished">Nom d’utilisateur (usagé1)</translation>
    </message>
    <message>
        <location filename="../qml/pages/LoginDialog.qml" line="50"/>
        <source>Password</source>
        <translation type="unfinished">Mot de passe</translation>
    </message>
</context>
<context>
    <name>Messages</name>
    <message>
        <location filename="../qml/pages/Messages.qml" line="36"/>
        <location filename="../qml/pages/Messages.qml" line="39"/>
        <source>File saved</source>
        <translation type="unfinished">Fichier enregistré</translation>
    </message>
    <message>
        <location filename="../qml/pages/Messages.qml" line="37"/>
        <source>File saved to Downloads directory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Messages.qml" line="125"/>
        <source>Copy</source>
        <translation type="unfinished">Copier</translation>
    </message>
    <message>
        <location filename="../qml/pages/Messages.qml" line="131"/>
        <source>Reply</source>
        <translation type="unfinished">Répondre</translation>
    </message>
    <message>
        <location filename="../qml/pages/Messages.qml" line="134"/>
        <source>Replying to </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Messages.qml" line="147"/>
        <source>Edit</source>
        <translation type="unfinished">Modifier</translation>
    </message>
    <message>
        <location filename="../qml/pages/Messages.qml" line="165"/>
        <source>Delete</source>
        <translation type="unfinished">Supprimer</translation>
    </message>
    <message>
        <location filename="../qml/pages/Messages.qml" line="255"/>
        <source>Download audio</source>
        <translation type="unfinished">Télécharger audio</translation>
    </message>
    <message>
        <location filename="../qml/pages/Messages.qml" line="287"/>
        <location filename="../qml/pages/Messages.qml" line="307"/>
        <source>View video</source>
        <translation type="unfinished">Voir la vidéo</translation>
    </message>
    <message>
        <location filename="../qml/pages/Messages.qml" line="287"/>
        <source>Download</source>
        <translation type="unfinished">Télécharger</translation>
    </message>
    <message>
        <location filename="../qml/pages/Messages.qml" line="316"/>
        <source>Download file</source>
        <translation type="unfinished">Télécharger le fichier</translation>
    </message>
    <message>
        <location filename="../qml/pages/Messages.qml" line="409"/>
        <source>Message</source>
        <translation type="unfinished">Message</translation>
    </message>
    <message>
        <location filename="../qml/pages/Messages.qml" line="410"/>
        <source>Send message to room</source>
        <translation type="unfinished">Envoyer messages au salon</translation>
    </message>
    <message>
        <location filename="../qml/pages/Messages.qml" line="440"/>
        <source>New Messages</source>
        <translation type="unfinished">Nouveau message</translation>
    </message>
    <message>
        <location filename="../qml/pages/Messages.qml" line="461"/>
        <source>Send file</source>
        <translation type="unfinished">Envoyer fichier</translation>
    </message>
    <message>
        <location filename="../src/messages.cpp" line="1288"/>
        <source> invited </source>
        <translation type="unfinished"> a invité </translation>
    </message>
    <message>
        <location filename="../src/messages.cpp" line="1292"/>
        <source> changed their profile</source>
        <translation type="unfinished"> a modifié son profil</translation>
    </message>
    <message>
        <location filename="../src/messages.cpp" line="1294"/>
        <source> joined</source>
        <translation type="unfinished"> a rejoint</translation>
    </message>
</context>
<context>
    <name>PictureDisplay</name>
    <message>
        <location filename="../qml/pages/PictureDisplay.qml" line="17"/>
        <location filename="../qml/pages/PictureDisplay.qml" line="20"/>
        <source>Image saved</source>
        <translation type="unfinished">Image enregistrée</translation>
    </message>
    <message>
        <location filename="../qml/pages/PictureDisplay.qml" line="18"/>
        <source>Image saved to Pictures directory</source>
        <translation type="unfinished">Image enregistrée dans le dossier Images</translation>
    </message>
    <message>
        <location filename="../qml/pages/PictureDisplay.qml" line="26"/>
        <location filename="../qml/pages/PictureDisplay.qml" line="29"/>
        <source>Image error</source>
        <translation type="unfinished">Erreur d'image</translation>
    </message>
    <message>
        <location filename="../qml/pages/PictureDisplay.qml" line="27"/>
        <source>Image not saved to Pictures directory</source>
        <translation type="unfinished">Image non enregistrée dans le dossier Images</translation>
    </message>
    <message>
        <location filename="../qml/pages/PictureDisplay.qml" line="51"/>
        <source>Save to Pictures</source>
        <translation type="unfinished">Enregistrer l'image dans</translation>
    </message>
</context>
<context>
    <name>RoomDirectory</name>
    <message>
        <location filename="../qml/pages/RoomDirectory.qml" line="28"/>
        <source>Explore public rooms</source>
        <translation type="unfinished">Explorer les salons publics</translation>
    </message>
    <message>
        <location filename="../qml/pages/RoomDirectory.qml" line="33"/>
        <source>Find a room...</source>
        <translation type="unfinished">Trouver un salon...</translation>
    </message>
    <message>
        <location filename="../qml/pages/RoomDirectory.qml" line="46"/>
        <source>Search in</source>
        <translation type="unfinished">Chercher dans</translation>
    </message>
    <message>
        <location filename="../qml/pages/RoomDirectory.qml" line="49"/>
        <source>My homeserver</source>
        <translation type="unfinished">Mon homeserver</translation>
    </message>
    <message>
        <location filename="../qml/pages/RoomDirectory.qml" line="50"/>
        <source>Matrix.org</source>
        <translation type="unfinished">Matrix.org</translation>
    </message>
    <message>
        <location filename="../qml/pages/RoomDirectory.qml" line="60"/>
        <source>No results</source>
        <translation type="unfinished">Aucun résultat</translation>
    </message>
    <message>
        <location filename="../qml/pages/RoomDirectory.qml" line="61"/>
        <source>Please try a different search term</source>
        <translation type="unfinished">Essayez un terme de recherche différent</translation>
    </message>
    <message>
        <location filename="../qml/pages/RoomDirectory.qml" line="71"/>
        <source>Search for a room</source>
        <translation type="unfinished">Chercher un salon</translation>
    </message>
    <message>
        <location filename="../qml/pages/RoomDirectory.qml" line="72"/>
        <source>Use the search box to search for a room</source>
        <translation type="unfinished">Utilisez la barre de rechercher per trouver des salons</translation>
    </message>
</context>
<context>
    <name>RoomsDisplay</name>
    <message>
        <location filename="../qml/custom/RoomsDisplay.qml" line="43"/>
        <source>Settings</source>
        <translation type="unfinished">Paramètres</translation>
    </message>
    <message>
        <location filename="../qml/custom/RoomsDisplay.qml" line="48"/>
        <source>Explore public rooms</source>
        <translation type="unfinished">Explorer les salons</translation>
    </message>
    <message>
        <location filename="../qml/custom/RoomsDisplay.qml" line="54"/>
        <source>Create new room</source>
        <translation type="unfinished">Créer un nouveau salon</translation>
    </message>
    <message>
        <location filename="../qml/custom/RoomsDisplay.qml" line="66"/>
        <source>No </source>
        <translation type="unfinished">Non </translation>
    </message>
    <message>
        <location filename="../qml/custom/RoomsDisplay.qml" line="82"/>
        <source>Leave</source>
        <translation type="unfinished">Quitter</translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <location filename="../qml/pages/Settings.qml" line="56"/>
        <source>Settings</source>
        <translation type="unfinished">Paramètres</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="60"/>
        <source>Notifications</source>
        <translation type="unfinished">Notifications</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="65"/>
        <source>Enable notifications</source>
        <translation type="unfinished">Activer les notifications</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="72"/>
        <source>Enable notifications when app is closed</source>
        <translation type="unfinished">Activer les notifications quand l’application est fermée</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="80"/>
        <source>Notification checking interval</source>
        <translation type="unfinished">Interval de vérification des notifications</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="82"/>
        <source>30 seconds</source>
        <translation type="unfinished">30 secondes</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="83"/>
        <source>2.5 minutes</source>
        <translation type="unfinished">2,5 minutes</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="84"/>
        <source>5 minutes</source>
        <translation type="unfinished"><5 minutes/translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="85"/>
        <source>10 minutes</source>
        <translation type="unfinished">10 minutes</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="86"/>
        <source>15 minutes</source>
        <translation type="unfinished">15 minutes</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="87"/>
        <source>30 minutes</source>
        <translation type="unfinished">30 minutes</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="88"/>
        <source>1 hour</source>
        <translation type="unfinished">1 heure</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="89"/>
        <source>2 hours</source>
        <translation type="unfinished">2 heures</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="90"/>
        <source>4 hours</source>
        <translation type="unfinished">4 heures</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="91"/>
        <source>8 hours</source>
        <translation type="unfinished"><8 heures/translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="92"/>
        <source>10 hours</source>
        <translation type="unfinished">10 heures</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="93"/>
        <source>12 hours</source>
        <translation type="unfinished">12 heures</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="94"/>
        <source>24 hours</source>
        <translation type="unfinished">24 heures</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="104"/>
        <source>Display</source>
        <translation type="unfinished">Affichage</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="109"/>
        <source>Sort rooms by</source>
        <translation type="unfinished">Tirer les salons par</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="111"/>
        <source>Activity</source>
        <translation type="unfinished">Activité</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="112"/>
        <source>Alphabetical</source>
        <translation type="unfinished">Alphabétiquement</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="120"/>
        <source>Global</source>
        <translation type="unfinished">Global</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="124"/>
        <source>Clear cache</source>
        <translation type="unfinished">Effacer le cache</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="125"/>
        <source>Cleared cache</source>
        <translation type="unfinished">Cache effacé</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="129"/>
        <source>Logout</source>
        <translation type="unfinished">Déconnexion</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="131"/>
        <source>Logging out</source>
        <translation type="unfinished">Déconnexion en cours</translation>
    </message>
</context>
<context>
    <name>Start</name>
    <message>
        <location filename="../qml/pages/Start.qml" line="22"/>
        <source>About</source>
        <translation type="unfinished">À prepos</translation>
    </message>
    <message>
        <location filename="../qml/pages/Start.qml" line="40"/>
        <source>Welcome</source>
        <translation type="unfinished">Bienvenue</translation>
    </message>
    <message>
        <location filename="../qml/pages/Start.qml" line="44"/>
        <source>Log in</source>
        <translation type="unfinished">Connexion</translation>
    </message>
</context>
<context>
    <name>User</name>
    <message>
        <location filename="../qml/pages/User.qml" line="83"/>
        <source>Direct Message</source>
        <translation type="unfinished">Message direct</translation>
    </message>
    <message>
        <location filename="../qml/pages/User.qml" line="94"/>
        <source>Ignore</source>
        <translation type="unfinished">Ignorer</translation>
    </message>
    <message>
        <location filename="../qml/pages/User.qml" line="104"/>
        <source>Unignore</source>
        <translation type="unfinished">Ne plus ignorer</translation>
    </message>
</context>
<context>
    <name>VideoPage</name>
    <message>
        <location filename="../qml/pages/VideoPage.qml" line="17"/>
        <location filename="../qml/pages/VideoPage.qml" line="20"/>
        <source>Video saved</source>
        <translation type="unfinished">Vidéo enregistrée</translation>
    </message>
    <message>
        <location filename="../qml/pages/VideoPage.qml" line="18"/>
        <source>Video saved to Videos directory</source>
        <translation type="unfinished"><Vidéo enregistrée dans le dossier Vidéos/translation>
    </message>
    <message>
        <location filename="../qml/pages/VideoPage.qml" line="26"/>
        <location filename="../qml/pages/VideoPage.qml" line="29"/>
        <source>Video error</source>
        <translation type="unfinished">Erreur de vidéo</translation>
    </message>
    <message>
        <location filename="../qml/pages/VideoPage.qml" line="27"/>
        <source>Video not saved to Videos directory</source>
        <translation type="unfinished">Vidéo non enregistrée dans le dossier Vidéos</translation>
    </message>
</context>
</TS>
